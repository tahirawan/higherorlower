<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body class="">

<div class="container ">

    <h1>Higher or Lower!</h1>
    <div class="row">
        <div class="col text-center">

            <div class="row">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $card->suit }}</h5>
                            <p class="card-text">{{ $card->value }}</p>
                            <p class="card-text">Remaining Lives {{ $remaining_lives }}</p>
                            @if ( $remaining_lives < 1 )
                                <p class="card-text">Correct Guesses {{ $guesses }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @if ( $remaining_lives < 1 )
                    <div class="col-sm-8">
                        <form action="/cards" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="_method" value="GET">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary">New Game</button>
                            </div>
                        </form>
                    </div>

                @else
                    <div class="col-sm-4">
                        <form action="/cards?card={{$guesses + 1}}&guess=higher" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="_method" value="GET">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary">Higher</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4">
                        <form action="/cards?card={{$guesses + 1}}&guess=lower" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="_method" value="GET">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary">Lower</button>
                            </div>
                        </form>
                        @endif
                    </div>
            </div>
        </div>
    </div>
</body>
</html>
