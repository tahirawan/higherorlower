
## 1 - run using artisan serve
    php artisan serve
- Page link e.g `http://127.0.0.1:8000`

## 2 - How to run the project Using docker
I have pushed docker envs as well. You can run using docker as well
docker and docker-compose should be installed

    docker-composer up -d

### to install composer libraries
    docker-compose exec app  bash
    composer install
### NOTE please don't forget to add .env
    cp .env.example .env
    php artisan key:generate

- Page link e.g `http://127.0.0.1:8080`

## Files Added
I have added `Components` folders under `app`. Which will contains all components. e.g `HigherOrLower`
  - `DavidNealCardsService` is to pull cards from external api
  - `HigherLowerCardsService` is containing the main logic of shuffling the cards and comparing user guesses


### Routes 

[https://bitbucket.org/tahirawan/higherorlower/src/master/routes/web.php](https://bitbucket.org/tahirawan/higherorlower/src/master/routes/web.php)

### Other Filese added
[https://bitbucket.org/tahirawan/higherorlower/src/master/app/Components/HigherLower/](https://bitbucket.org/tahirawan/higherorlower/src/master/app/Components/HigherLower/)

[https://bitbucket.org/tahirawan/higherorlower/src/master/app/Http/Controllers/ApiController.php](https://bitbucket.org/tahirawan/higherorlower/src/master/app/Http/Controllers/ApiController.php)

[https://bitbucket.org/tahirawan/higherorlower/src/master/app/Http/Controllers/CardController.php](https://bitbucket.org/tahirawan/higherorlower/src/master/app/Http/Controllers/CardController.php)

[https://bitbucket.org/tahirawan/higherorlower/src/master/config/david-neal-cards.php](https://bitbucket.org/tahirawan/higherorlower/src/master/config/david-neal-cards.php)

[https://bitbucket.org/tahirawan/higherorlower/src/master/resources/views/cards.blade.php](https://bitbucket.org/tahirawan/higherorlower/src/master/resources/views/cards.blade.php)
