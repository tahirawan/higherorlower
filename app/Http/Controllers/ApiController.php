<?php

declare(strict_types=1);

namespace App\Http\Controllers;

class ApiController extends Controller
{
    /**
     * @param $data
     * @param int $statusCode
     * @param array $headers
     * @param int $options
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResponse($data = null, int $statusCode, array $headers = [], $options = 0)
    {
        return response()->json($data, $statusCode, $headers, $options);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondSuccess($data)
    {
        return $this->sendResponse($data, 200);
    }

    /**
     * @param string|null $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInternalError($message = null)
    {
        return response()->json($message, 500);
    }
}
