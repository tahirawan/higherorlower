<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Components\HigherLower\Services\HigherLowerCardsService;
use Illuminate\Http\Request;
use Log;
use Throwable;

class CardController extends ApiController
{
    private $service;


    public function __construct(HigherLowerCardsService $higherLowerCardsService)
    {
        $this->service = $higherLowerCardsService;
    }

    /**
     * Listing shuffled cards.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $cardNumber = $request->input('card');
            $guess = $request->input('guess');

            if(empty($cardNumber) || !in_array($guess, ['higher', 'lower'])) {
                $shuffleCards = $this->service->getFirstCard();
            }else{
                $shuffleCards = $this->service->checkUserGuess((int) $cardNumber, ($guess == 'higher'));
            }

            return response()->view('cards', $shuffleCards);

            return $this->respondSuccess($shuffleCards);
        } catch (Throwable $t) {
            dd($t);
            Log::error('Cards Error');
            return $this->respondInternalError();
        }
    }

    public function higherOrLower()
    {
        //$this->service
    }
}
