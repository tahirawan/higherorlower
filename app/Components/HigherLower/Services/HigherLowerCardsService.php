<?php

namespace App\Components\HigherLower\Services;

use Cache;
use App\Components\HigherLower\External\Services\DavidNealCardsService;
use Illuminate\Support\Collection;

class HigherLowerCardsService
{
    private $service;
    private $cacheKey = 'shuffled-cards';
    private $newGame = true;
    private $cardsGuessed = 0;
    private $maxLives = 3;

    public function __construct(DavidNealCardsService $davidNealCardsService)
    {
        $this->service = $davidNealCardsService;
    }

    public function getFirstCard()
    {
        cache()->clear();

        $cards = $this->getShuffledCards();

        return $this->transform($cards->first(), $this->maxLives);
    }

    public function checkUserGuess(int $cardNumber, bool $isHigher)
    {
        $this->cardsGuessed = $cardNumber;
        $this->newGame = false;

        $shuffled = $this->getShuffledCards();

        $nextCard = $this->getNextCard($shuffled);

        $validGuess = $this->isValidGuess($this->getCurrentCard($shuffled), $nextCard, $isHigher);

        $incorrectAttempts = $this->getIncorrectAttempts();
        if (!$validGuess) {
            $incorrectAttempts = $this->incrementIncorrectAttempts();
        }

        return $this->transform($nextCard, ($this->maxLives - $incorrectAttempts));
    }

    private function getCurrentCard($shuffled)
    {
        if ($this->cardsGuessed < 1) {
            return $shuffled->first();
        }

        return $shuffled->take($this->cardsGuessed)->last();
    }

    private function getIncorrectAttempts(): int
    {
        $incorrectAttempts = Cache::get($this->getIncorrectAttepmtKey());
        if (empty($incorrectAttempts)) {
            $incorrectAttempts = 0;
        }

        return $incorrectAttempts;
    }

    private function incrementIncorrectAttempts(): int
    {
        $incorrectAttempts = $this->getIncorrectAttempts();

        ++$incorrectAttempts;

        Cache::put($this->getIncorrectAttepmtKey(), $incorrectAttempts, 60);

        return $incorrectAttempts;
    }

    private function getIncorrectAttepmtKey(): string
    {
        return 'lives-limit1';
    }

    private function getNextCard($shuffled)
    {
        $remainingCards = $this->removedGuessed($shuffled);

        return $remainingCards->first();
    }

    private function isValidGuess($userCard, $nextCard, bool $isHigher): bool
    {
        if ($isHigher) {
            return $this->getCardValue($userCard) < $this->getCardValue($nextCard);
        }

        return $this->getCardValue($userCard) > $this->getCardValue($nextCard);
    }

    private function getCardValue($card)
    {
        switch ($card->value) {
            case 'A':
                $cardValue = 1;
                break;
            case 'J':
                $cardValue = 11;
                break;
            case 'Q':
                $cardValue = 12;
                break;
            case 'K':
                $cardValue = 13;
                break;
            default:
                $cardValue = $card->value;

        }

        return (int)$cardValue;
    }

    private function removedGuessed($cards): Collection
    {
        if ($this->cardsGuessed == 0) {
            return $cards->slice(1);
        }

        return $cards->slice($this->cardsGuessed);
    }

    private function getShuffledCards(): Collection
    {
        $shuffledCards = !$this->newGame ? Cache::get($this->cacheKey) : null;
        if (!empty($shuffledCards)) {
            return $shuffledCards;
        }

        $cards = collect($this->service->getCards());

        $shuffled = $cards->shuffle();

        Cache::put($this->cacheKey, $shuffled, 60);

        return $shuffled;
    }

    private function transform($card, int $remainingLives)
    {
        return [
            'card' => $card,
            'guesses' => $this->cardsGuessed,
            'remaining_lives' => $remainingLives
        ];
    }
}