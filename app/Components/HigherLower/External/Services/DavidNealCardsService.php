<?php

namespace App\Components\HigherLower\External\Services;

use Config;
use Exception;
use GuzzleHttp\Client;
use Log;

class DavidNealCardsService
{

    public function getCards()
    {
        $url = $this->getApiUrl();

        $client = new Client();
        try {
            $response = $client->request('get', $url);
            $bodyContent = $response->getBody()->getContents();

            return empty($bodyContent) ? [] : json_decode($bodyContent);
        } catch (Exception $e) {
            Log::info('David cards service Exception', [
                ([
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ]),
            ]);
            throw $e;
        }
    }

    /**
     * @return string
     */
    private function getApiUrl(): string
    {
        return Config::get('david-neal-cards.url');
    }

}